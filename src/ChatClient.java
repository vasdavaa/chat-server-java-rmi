
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class ChatClient extends UnicastRemoteObject implements IChatClient {
	private static final long serialVersionUID = 5428926780936515390L;
	private String name;
	private ChatUI ui;

	public ChatClient(String n) throws RemoteException {
		name = n;
	}

	public void tell(String st) throws RemoteException {
		System.out.println(st);
		ui.writeMsg(st);
	}

	public String getName() throws RemoteException {
		return name;
	}

	public void setGUI(ChatUI t) {
		ui = t;
	}

	public void updateUserListForUser(List<String> userNames) throws RemoteException {
		ui.updateUserListOnUI(userNames);
	}
}