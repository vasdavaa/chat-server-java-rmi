
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class ChatServer extends UnicastRemoteObject implements IChatServer {
	private static final long serialVersionUID = 4705993250126188735L;
	private ArrayList<IChatClient> v = new ArrayList<IChatClient>();

	public ChatServer() throws RemoteException {
	}

	public boolean login(IChatClient client) throws RemoteException {
		System.out.println(client.getName() + " got connected....");
		client.tell("client connected");
		publish(client.getName() + " connected.");
		v.add(client);
		return true;
	}

	public void publish(String s) throws RemoteException {
		System.out.println(s);
		for (int i = 0; i < v.size(); i++) {
			try {
				IChatClient tmp = (IChatClient) v.get(i);
				tmp.tell(s);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<IChatClient> getConnected() throws RemoteException {
		return v;
	}

	/**
	 * Szerver megkapja a listát, átalakítja String listára ami a csatlakozott
	 * felhasználó neveket tartalmazza. Ezeket a neveket elküldi az összes
	 * csatlakozott kliensnek.
	 * 
	 * @param users a felhasználók listája
	 */
	public void userListUpdateRequest(ArrayList<IChatClient> users) throws RemoteException {
		List<String> userNames = new ArrayList<>();
		users.stream().forEach(user -> {
			try {
				userNames.add(user.getName());
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
		for (int i = 0; i < users.size(); i++) {
			IChatClient tmp = (IChatClient) users.get(i);
			tmp.updateUserListForUser(userNames);
		}
	}
}
